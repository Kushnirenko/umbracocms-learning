﻿using CmsLearning.Data.Models;
using System.Data.Entity;

namespace CmsLearning.Data.Repositories.Interfaces
{
    public interface ICmsLearningContext
    {
        DbSet<Candidate> Candidates { get; }
        int SaveChanges();
    }
}
