﻿using CmsLearning.Data.Models;

namespace CmsLearning.Data.Repositories.Interfaces
{
    public interface IRepositoriesFactory
    {
        IRepository<Candidate> CandidateRepository { get; }
    }
}
