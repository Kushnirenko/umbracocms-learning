﻿using CmsLearning.Data.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace CmsLearning.Data.Repositories.Interfaces
{
    public interface IRepository<TEntity> where TEntity : IEntity
    {
        IEnumerable<TEntity> Get();
        IEnumerable<TEntity> Where(Expression<Func<TEntity, bool>> expression);
        TEntity FirstOrDefault(Expression<Func<TEntity, bool>> expression);
        void Delete(int id);
        void Add(TEntity model);
    }
}
