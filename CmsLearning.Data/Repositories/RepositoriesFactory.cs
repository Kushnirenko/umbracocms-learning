﻿using CmsLearning.Data.Models;
using CmsLearning.Data.Repositories.Interfaces;

namespace CmsLearning.Data.Repositories
{
    public class RepositoriesFactory : IRepositoriesFactory
    {
        private readonly CmsLearningContext _context;

        private IRepository<Candidate> _candidateRepository;

        public RepositoriesFactory()
        {
            _context = new CmsLearningContext();
        }

        public IRepository<Candidate> CandidateRepository
        {
            get
            {
                if (_candidateRepository == null)
                {
                    _candidateRepository = new Repository<Candidate>(_context.Candidates, _context);
                }

                return _candidateRepository;
            }
        }
    }
}
