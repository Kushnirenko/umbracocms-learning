﻿using CmsLearning.Data.Models;
using CmsLearning.Data.Models.Interfaces;
using CmsLearning.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace CmsLearning.Data.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        private readonly DbSet<TEntity> _dbSet;
        private readonly CmsLearningContext _context;

        public Repository(DbSet<TEntity> dbSet, CmsLearningContext context)
        {
            _dbSet = dbSet;
            _context = context;
        }

        #region IGenericRepository<T> implementation

        public IEnumerable<TEntity> Get()
        {
            return _dbSet.ToArray();
        }

        public IEnumerable<TEntity> Where(Expression<Func<TEntity, bool>> expression)
        {
            return _dbSet.Where(expression);
        }

        public TEntity FirstOrDefault(Expression<Func<TEntity, bool>> expression)
        {
            return _dbSet.FirstOrDefault(expression);
        }

        public void Delete(int id)
        {
            var item = _dbSet.Single(o => o.Id == id);
            _dbSet.Remove(item);
            _context.SaveChanges();
        }

        public void Add(TEntity model)
        {
            _dbSet.Add(model);
            _context.SaveChanges();
        }
        #endregion
    }
}
