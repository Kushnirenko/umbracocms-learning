﻿using CmsLearning.Data.Models;
using CmsLearning.Data.Repositories.Interfaces;
using System.Data.Entity;

namespace CmsLearning.Data.Repositories
{
    public class CmsLearningContext : DbContext, ICmsLearningContext
    {
        public CmsLearningContext()
            : base("umbracoDbDSN")
        {
        }

        public override int SaveChanges()
        {
            return base.SaveChanges();
        }

        public DbSet<Candidate> Candidates { get; }
    }
}
