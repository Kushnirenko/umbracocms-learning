﻿using System;
using CmsLearning.Data.Models.Interfaces;

namespace CmsLearning.Data.Models
{
    public class Entity : IEntity
    {
        public int Id { get; set; }
    }
}
