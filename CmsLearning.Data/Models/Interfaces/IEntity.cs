﻿namespace CmsLearning.Data.Models.Interfaces
{
    public interface IEntity
    {
        int Id { get; }
    }
}
