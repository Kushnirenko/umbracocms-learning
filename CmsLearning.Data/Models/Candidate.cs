﻿using CmsLearning.Data.Models.Interfaces;
using System;

namespace CmsLearning.Data.Models
{
    public class Candidate : Entity
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string CVFileUrl { get; set; }
        public string CoveringLetterFileUrl { get; set; }
        public DateTime RequestedOn { get; set; }
    }
}
