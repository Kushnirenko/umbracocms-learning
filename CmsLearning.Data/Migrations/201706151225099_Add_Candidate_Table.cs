namespace CmsLearning.Data.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Add_Candidate_Table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Candidates",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    FullName = c.String(),
                    Email = c.String(),
                    CVFileUrl = c.String(),
                    CoveringLetterFileUrl = c.String(),
                    RequestedOn = c.DateTime(nullable: false),
                })
                .PrimaryKey(t => t.Id);
        }

        public override void Down()
        {
            DropTable("dbo.Candidates");
        }
    }
}
