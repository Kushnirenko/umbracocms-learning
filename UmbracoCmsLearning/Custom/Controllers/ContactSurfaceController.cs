﻿using Umbraco.Web.Mvc;
using System.Web.Mvc;
using UmbracoCmsLearning.Custom.Models;
using System.Net.Mail;
using System.Net.Mime;
using UmbracoCmsLearning.Custom.BL;
using UmbracoCmsLearning.Custom.BL.Models;
using System;
using System.Web;
using System.IO;
using Umbraco.Core.Models;

namespace UmbracoCmsLearning.Custom.Controllers
{
    public class ContactSurfaceController : SurfaceController
    {
        private const string PARTIAL_VIEW_FOLDER = "~/Views/Partials/Contact/";
        private readonly CandidateService _candidateService;
        private const string CV_FILE_FIELD = "CVFile";
        private const string COVERING_NOTE_FIELD = "CoveringNoteFile";
        private const string MEDIA_FOLDER = "media";
        private const string CANDIDATE_ATTACHMENT_FOLDER = "CandidateAttachments";

        public ContactSurfaceController()
        {
            _candidateService = new CandidateService();
        }

        public ActionResult RenderForm()
        {
            return PartialView(PARTIAL_VIEW_FOLDER + "_Contact.cshtml");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitForm(ContactModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _candidateService.Insert(MapCandidateModel(model));
                    TempData["ContactSuccess"] = true;
                    SendEmail(model);
                }
                catch (Exception ex)
                {
                    TempData["ContactSuccess"] = false;
                    //TODO: Implement logging logic
                }

                return RedirectToCurrentUmbracoPage();
            }
            return View(model);
        }

        private Candidate MapCandidateModel(ContactModel model)
        {
            var candidateId = _candidateService.GetIdForNewItem();
            string cVFileUrl = SaveMediaFiles(model.CVFile, candidateId);
            string coveringNoteFileUrl = SaveMediaFiles(model.CoveringNoteFile, candidateId);

            return new Candidate
            {
                Id = candidateId,
                Email = model.EmailAddress,
                FullName = $"{model.FirstName} {model.LastName}",
                RequestedOn = DateTime.UtcNow,
                CoveringNoteFileUrl = coveringNoteFileUrl,
                CVFileUrl = cVFileUrl
            };
        }

        private string SaveMediaFiles(HttpPostedFileBase file, int candidateId)
        {
            if (file.ContentLength > 0)
            {
                string fileName = Path.GetFileName(file.FileName);
                string attachmentsFolder = Path.Combine(Server.MapPath(""), @"..\", MEDIA_FOLDER, CANDIDATE_ATTACHMENT_FOLDER);

                if (!Directory.Exists(attachmentsFolder))
                {
                    Directory.CreateDirectory(attachmentsFolder);
                }

                string destinationFolder = Path.Combine(attachmentsFolder, candidateId.ToString());
                Directory.CreateDirectory(destinationFolder);

                string fileSavePath = Path.Combine(destinationFolder, fileName);
                file.SaveAs(fileSavePath);

                return $"/{MEDIA_FOLDER}/{CANDIDATE_ATTACHMENT_FOLDER}/{candidateId}/{fileName}";
            }
            else
            {
                return null;
            }
        }

        private void SendEmail(ContactModel model)
        {
            MailMessage message = new MailMessage(model.EmailAddress, "Papercut@user.com");

            var files = Request.Files;
            string info = string.Empty;
            var fileKeys = new string[] { CV_FILE_FIELD, COVERING_NOTE_FIELD };
            for (int i = 0; i < fileKeys.Length; i++)
            {
                var file = Request.Files[fileKeys[i]];
                if (file.ContentLength > 0)
                {
                    var attachment = new Attachment(file.InputStream, file.ContentType);
                    attachment.ContentType = new System.Net.Mime.ContentType(file.ContentType);
                    attachment.Name = file.FileName;
                    message.Attachments.Add(attachment);
                }
            }

            message.Subject = string.Format("Enquiry from {0} {1} - {2}", model.FirstName, model.LastName, model.EmailAddress);
            message.Body = model.Message;
            SmtpClient client = new SmtpClient("127.0.0.1", 25);
            client.Send(message);
        }
    }
}