﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web;
using umbraco.BusinessLogic.Actions;
using Umbraco.Core;
using Umbraco.Web.Models.Trees;
using Umbraco.Web.Mvc;
using Umbraco.Web.Trees;
using UmbracoCmsLearning.Custom.Extensions.JobRequests;

namespace UmbracoCmsLearning.Custom.Extensions
{
    [Tree("jobRequests", "jobRequestsTree", "Job Requests")]
    [PluginController("JobRequests")]
    public class JobRequestTreeController : TreeController
    {
        private readonly string DEFAULT_PARENT_ID = "-1";

        protected override MenuItemCollection GetMenuForNode(string id, FormDataCollection queryStrings)
        {
            var menu = new MenuItemCollection();

            if (id == Constants.System.Root.ToInvariantString())
            {
                // root actions
                menu.Items.Add<RefreshNode, ActionRefresh>(
                    ApplicationContext.Services.TextService.Localize(ActionRefresh.Instance.Alias, CultureInfo.CurrentCulture));
            }
            if (id != "-1")
            {
                menu.Items.Add<ActionDelete>("Delete");
            }

            return menu;
        }

        protected override TreeNodeCollection GetTreeNodes(string id, FormDataCollection queryStrings)
        {
            var nodes = new TreeNodeCollection();
            var controller = new JobRequestsApiController();
            var candidateNodes = controller.GetAll().Select(x =>
            {
                var node = CreateTreeNode(x.Id.ToString(), DEFAULT_PARENT_ID, queryStrings, x.FullName, "icon-people");

                return node;
            });
            nodes.AddRange(candidateNodes);
            return nodes;
        }
    }
}