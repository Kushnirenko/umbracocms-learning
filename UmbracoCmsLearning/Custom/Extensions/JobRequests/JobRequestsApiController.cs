﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Mvc;
using Umbraco.Web.Editors;
using Umbraco.Web.Mvc;
using UmbracoCmsLearning.Custom.BL;
using UmbracoCmsLearning.Custom.BL.Models;

namespace UmbracoCmsLearning.Custom.Extensions.JobRequests
{
    [PluginController("JobRequests")]
    public class JobRequestsApiController : UmbracoAuthorizedJsonController
    {
        private readonly CandidateService _candidateService;

        public JobRequestsApiController()
        {
            _candidateService = new CandidateService();
        }

        [HttpGet]
        public IEnumerable<Candidate> GetAll()
        {
            return _candidateService.GetAll();
        }

        [HttpGet]
        public HttpResponseMessage GetById(int id)
        {
            var candidate = _candidateService.GetById(id);
            if (candidate != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, candidate);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Candidate does not exist");
            }
        }


        [HttpDelete]
        public void Delete(int id)
        {
            _candidateService.Delete(id);
        }
    }
}