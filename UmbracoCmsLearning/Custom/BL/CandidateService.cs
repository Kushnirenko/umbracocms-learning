﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UmbracoCmsLearning.Custom.BL.Models;
using UmbracoCmsLearning.Custom.DAL.Repositories;

namespace UmbracoCmsLearning.Custom.BL
{
    public class CandidateService
    {
        private readonly CandidateRepository _candidateRepository;

        public CandidateService()
        {
            _candidateRepository = new CandidateRepository();
        }

        public IEnumerable<Candidate> GetAll()
        {
            return _candidateRepository.GetAll()
                .Select(candidate => new Candidate
                {
                    CoveringNoteFileUrl = candidate.CoveringLetterFileUrl,
                    CVFileUrl = candidate.CVFileUrl,
                    Email = candidate.Email,
                    FullName = candidate.FullName,
                    Id = candidate.Id
                });
        }

        public Candidate GetById(int id)
        {
            var candidate = _candidateRepository.GetById(id);

            if (candidate == null)
            {
                return null;
            }

            return new Candidate
            {
                CoveringNoteFileUrl = candidate.CoveringLetterFileUrl,
                CVFileUrl = candidate.CVFileUrl,
                Email = candidate.Email,
                FullName = candidate.FullName,
                Id = candidate.Id,
                RequestedOn = candidate.RequestedOn
            };
        }

        public void Insert(Candidate candidate)
        {
            _candidateRepository.Insert(new DAL.Entities.CandidateEntity
            {
                CoveringLetterFileUrl = candidate.CoveringNoteFileUrl,
                CVFileUrl = candidate.CVFileUrl,
                Email = candidate.Email,
                FullName = candidate.FullName,
                RequestedOn = candidate.RequestedOn
            });
        }

        public int GetIdForNewItem()
        {
            return _candidateRepository.GetIdForNewItem();
        }

        public void Delete(int id)
        {
            _candidateRepository.Delete(id);
        }


    }
}