﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UmbracoCmsLearning.Custom.BL.Models
{
    public class Candidate
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string CVFileUrl { get; set; }
        public string CoveringNoteFileUrl { get; set; }
        public DateTime RequestedOn { get; set; }
        public string RequestedOnDisplay
        {
            get
            {
                return (RequestedOn != null) ? RequestedOn.ToString(@"MMM/dd/yyyy H:mm:ss") : string.Empty;
            }
            set
            {
            }
        }
    }
}