﻿using Umbraco.Core.Logging;
using Umbraco.Core;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.Migrations;
using Umbraco.Core.Persistence.SqlSyntax;
using UmbracoCmsLearning.Custom.DAL.Entities;

namespace UmbracoCmsLearning.Custom.DAL.Migrations
{
    [Migration("1.0.0", 1, "Candidate")]
    public class CreateCandidateTable : MigrationBase
    {
        private readonly UmbracoDatabase _database = ApplicationContext.Current.DatabaseContext.Database;
        private readonly DatabaseSchemaHelper _schemaHelper;

        public CreateCandidateTable(ISqlSyntaxProvider sqlSyntax, ILogger logger)
            : base(sqlSyntax, logger)
        {
            _schemaHelper = new DatabaseSchemaHelper(_database, logger, sqlSyntax);
        }

        public override void Up()
        {
            _schemaHelper.CreateTable<CandidateEntity>(false);
        }

        public override void Down()
        {
            _schemaHelper.DropTable<CandidateEntity>();
        }
    }
}