﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Persistence;

namespace UmbracoCmsLearning.Custom.DAL.Repositories
{
    public class BaseRepository<T> where T : IEntity
    {
        protected UmbracoDatabase Connection { get; }
        private const int DEFAULT_ID = -1;

        public BaseRepository()
        {
            Connection = ApplicationContext.Current.DatabaseContext.Database;
        }

        public IEnumerable<T> GetAll()
        {
            var query = new Sql()
                .Select("*")
                .From<T>();

            return Connection.Fetch<T>(query);
        }

        public void Delete(int id)
        {
            var itemForDelete = Connection.FirstOrDefault<T>(GetWhereQuery(x => x.Id == id));
            Connection.Delete(itemForDelete);
        }

        public T GetById(int id)
        {
          return  Connection.FirstOrDefault<T>(GetWhereQuery(o => o.Id == id));
        }

        private Sql GetWhereQuery(Expression<Func<T, bool>> expression)
        {
            return new Sql().Select("*").From<T>().Where(expression);
        }

        public void Update(T model)
        {
            Connection.Update(model);
        }

        public int Insert(T model)
        {
            if (model != null)
            {
                Connection.Insert(model);
                return model.Id;
            }

            return DEFAULT_ID;
        }
    }
}