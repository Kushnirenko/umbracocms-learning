﻿using Umbraco.Core.Persistence;
using UmbracoCmsLearning.Custom.DAL.Entities;

namespace UmbracoCmsLearning.Custom.DAL.Repositories
{
    public class CandidateRepository : BaseRepository<CandidateEntity>
    {
        public int GetIdForNewItem()
        {
            return Connection.FirstOrDefault<int>(new Sql("SELECT TOP(1) (Id + 1) AS Id FROM Candidate ORDER BY Id DESC"));
        }
    }
}