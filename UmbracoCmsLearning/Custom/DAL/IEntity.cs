﻿namespace UmbracoCmsLearning.Custom.DAL
{
    public interface IEntity
    {
        int Id { get; }
    }
}
