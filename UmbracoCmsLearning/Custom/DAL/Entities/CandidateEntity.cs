﻿using System;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace UmbracoCmsLearning.Custom.DAL.Entities
{
    [TableName("Candidate")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class CandidateEntity : IEntity
    {
        [PrimaryKeyColumn(AutoIncrement = true)]
        [Column("Id")]
        public int Id { get; set; }
        [Column("FullName")]
        public string FullName { get; set; }
        [Column("Email")]
        public string Email { get; set; }
        [Column("CVFileUrl")]
        public string CVFileUrl { get; set; }
        [Column("CoveringLetterFileUrl")]
        public string CoveringLetterFileUrl { get; set; }
        [Column("RequestedOn")]
        public DateTime RequestedOn { get; set; }
    }
}