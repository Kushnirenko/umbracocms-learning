﻿using System.ComponentModel.DataAnnotations;
using System.Web;

namespace UmbracoCmsLearning.Custom.Models
{
    public class ContactModel
    {
        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Email Address")]
        [EmailAddress]
        public string EmailAddress { get; set; }

        [Required]
        [Display(Name = "Message")]
        public string Message { get; set; }

        [Required]
        [Display(Name = "CVFile")]
        public HttpPostedFileBase CVFile { get; set; }

        [Required]
        [Display(Name = "Covering Note")]
        public HttpPostedFileBase CoveringNoteFile { get; set; }
    }
}