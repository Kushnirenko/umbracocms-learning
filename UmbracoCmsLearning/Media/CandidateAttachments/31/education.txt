�����������:

CLR via C# by Jeffrey Richter
C# in a Nutshell by Joseph Albahari, Ben Albahari
The Art of Computer Programming by Donald E. Knuth
Code Complete: A Practical Handbook of Software Construction by Steve McConnell
Agile Principles, Patterns, and Practices in C# by Robert C. Martin and Micah Martin
Design Patterns: Elements of Reusable Object-Oriented Software by Gamma, Helm, Johnson, Vlissides
Professional ASP.NET MVC by Jon Galloway, Brad Wilson, K. Scott Allen, David Matson
JavaScript: The Good Parts by Douglas Crockford


����������:

The Pragmatic Programmer: From Journeyman to Master ������� Andrew Hunt, Dave Thomas
Domain-Driven Design: Tackling Complexity in the Heart of Software ������ Eric Evans
Working Effectively with Legacy Code ������ Michael Feathers
Refactoring: Improving the Design of Existing Code by Fowler, Beck, Brant, Opdyke, Roberts
The Art of Unit Testing by Roy Osherove
Kanban and Scrum � Making the Most of Both by Henrik Kniberg,  Mattias Skarin
Scrum and XP from the Trenches by Henrik Kniberg
The Art of Readable Code by Dustin Boswell, Trevor Foucher
��������� ��� ������������:

http://mvcmusicstore.codeplex.com/
http://nerddinner.codeplex.com/
http://www.asp.net/
https://msdn.microsoft.com/en-us/
http://www.dofactory.com/
https://visualstudiomagazine.com/