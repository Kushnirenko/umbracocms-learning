﻿function DeleteJobRequestController($scope, jobRequestResource, treeService, navigationService, $route, $location) {

    $scope.onCancel = function () {
        navigationService.hideDialog();
    }

    $scope.onConfirm = function () {
        jobRequestResource.remove($scope.currentNode.id)
            .success(function (response) {
                treeService.removeNode($scope.currentNode);
                navigationService.hideMenu();
                navigationService.reloadSection();
                navigationService.hideNavigation();
                $location.path($scope.currentNode.section)
            })
            .error(function (errors) {
                alert("Something happen wrong")
            });
    }
}
angular.module("umbraco").controller("DeleteJobRequestController", DeleteJobRequestController);