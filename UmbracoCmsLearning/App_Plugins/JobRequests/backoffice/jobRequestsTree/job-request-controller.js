﻿angular.module("umbraco").controller("JobRequestsController", [
'$scope', '$routeParams', 'jobRequestResource', '$filter',
function ($scope, $routeParams, jobRequestResource, $filter) {
    $scope.id = $routeParams.id;
    $scope.details = null;
    $scope.confirmed = false;
    $scope.loading = true;

    var DEFAULT_ERROR_MESSAGE = 'Something happened wrong';

    function init() {
        $scope.loading = true;

        jobRequestResource.getById($scope.id)
        .success(function (response) {
            $scope.details = new Candidate(response.CVFileUrl,
                response.CoveringNoteFileUrl,
                response.Email,
                response.FullName,
                response.Id,
                $filter('date')(new Date(response.RequestedOn), 'MMM dd, yyyy HH:mm'));
            $scope.loading = false;
        })
        .error(function (error) {
            $scope.loading = false;
            $scope.errorMessage = error.Message ? error.Message : DEFAULT_ERROR_MESSAGE;
        });
    }

    function Candidate(cvFile, coveringLetterFileUrl, email, fullName, id, requestedOn) {
        this.cvFile = cvFile;
        this.coveringLetterFileUrl = coveringLetterFileUrl;
        this.email = email;
        this.fullName = fullName;
        this.id = id;
        this.requestedOn = requestedOn;
    }

    init();
}]);