﻿angular.module("umbraco.resources")
    .factory("jobRequestResource", function ($http) {
        var url = "backoffice/JobRequests/JobRequestsApi/"
        return {
            getById: function (id) {
                return $http.get(url + "GetById?id=" + id);
            },
            remove: function (id) {
                return $http.delete(url + "Delete?id=" + id);
            }
        };
    });