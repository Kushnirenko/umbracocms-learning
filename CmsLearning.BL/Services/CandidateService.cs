﻿using CmsLearning.BL.Mappers;
using CmsLearning.BL.Models;
using CmsLearning.Data.Repositories;
using CmsLearning.Data.Repositories.Interfaces;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CmsLearning.BL.Services
{
    public class CandidateService
    {
        private readonly IRepositoriesFactory _repositoriesFactory;
        private readonly CandidateMapper _candidateMapper;

        public CandidateService()
        {
            _repositoriesFactory = new RepositoriesFactory();
            _candidateMapper = new CandidateMapper();
        }

        public IEnumerable<Candidate> GetAll()
        {
            return _repositoriesFactory.CandidateRepository.Get()
                .Select(_candidateMapper.MapToBusinessModel);
        }

        public void Insert(Candidate candidate)
        {
            _repositoriesFactory.CandidateRepository.Add(_candidateMapper.MapToEntity(candidate));
        }

        public void Delete(int id)
        {
            _repositoriesFactory.CandidateRepository.Delete(id);
        }

        public Candidate GetById(int id)
        {
            var candidate = _repositoriesFactory.CandidateRepository.FirstOrDefault(o => o.Id == id);
            return candidate != null ? _candidateMapper.MapToBusinessModel(candidate) : null;
        }
    }
}
