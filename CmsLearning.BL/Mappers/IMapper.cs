﻿namespace CmsLearning.BL.Mappers
{
    public abstract class IMapper<TDataModel, TBusinessModel>
    {
        public abstract TDataModel MapToEntity(TBusinessModel model);
        public abstract TBusinessModel MapToBusinessModel(TDataModel model);
    }
}
