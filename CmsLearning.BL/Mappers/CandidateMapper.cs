﻿namespace CmsLearning.BL.Mappers
{
    public class CandidateMapper : IMapper<Data.Models.Candidate, Models.Candidate>
    {
        public override Models.Candidate MapToBusinessModel(Data.Models.Candidate model)
        {
            return new Models.Candidate
            {
                CoveringLetterFileUrl = model.CoveringLetterFileUrl,
                CVFileUrl = model.CVFileUrl,
                Email = model.Email,
                FullName = model.FullName,
                RequestedOn = model.RequestedOn
            };
        }

        public override Data.Models.Candidate MapToEntity(Models.Candidate model)
        {
            return new Data.Models.Candidate
            {
                CoveringLetterFileUrl = model.CoveringLetterFileUrl,
                CVFileUrl = model.CVFileUrl,
                Email = model.Email,
                FullName = model.FullName,
                RequestedOn = model.RequestedOn
            };
        }
    }
}
