﻿using System;

namespace CmsLearning.BL.Models
{
    public class Candidate
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string CVFileUrl { get; set; }
        public string CoveringLetterFileUrl { get; set; }
        public DateTime RequestedOn { get; set; }
    }
}
